#!/usr/bin/env bash
#title          : Build Ventoy Grub Theme
#description    : Generate the Ventoy Grub theme files and directory structure to current working directory under
#                 "ventoy/".
#date           : 2021-02-13
#usage          : build
#dependencies   : cp, find, inkscape, mkdir
#bash_version   : GNU bash, version 5.1.4(1)-release (x86_64-pc-linux-gnu)
declare -a dependencies=("cp" "find" "inkscape" "mkdir")

prepare() {
    if [[ -d "ventoy/" ]]
    then
        quit "Error: The build directory \"ventoy/\", already exists. Build has been halted."
    fi

    checkDepdenencies
}

checkDepdenencies() {
    for i in "${dependencies[@]}";
    do
        checkCmdExists "$i"
    done

    if [[ $cmdNotFound != '' ]]
    then
        quit "Error: The following command(s) where not found:${cmdNotFound::-1}."
    fi
}

checkCmdExists() {
    if ! [ -x "$(command -v $1)" ]
    then
        cmdNotFound="$cmdNotFound $1,"
    fi
}

quit() {
    if [[ "$1" = 0 ]]
    then
            exit 0
    else
            printf "%b\n" "$1"
            exit 1
    fi
}

build() {
    mkdir -p ./ventoy/theme/adamsdesk/icons || quit "Error: Unable to create directory \"ventoy/theme/adamsdesk/icons\". Build has been halted."
    cp ./src/fonts/*.pf2 ./ventoy/theme/adamsdesk/ || quit "Error: Unable to copy fonts. Build has been halted."
    cp ./src/menu/select_*.png ./ventoy/theme/adamsdesk/ || quit "Error: Unable to copy fonts. Build has been halted."
    cp ./src/config/theme.txt ./ventoy/theme/adamsdesk/ || quit "Error: Unable to copy fonts. Build has been halted."
    cp ./src/config/ventoy.json ./ventoy/ || quit "Error: Unable to copy fonts. Build has been halted."
    cd src/icons/ || quit "Error: Unable to change directory to \"src/icons\"."
    find -name "*.svg" ! -name adamsdesk.svg -exec sh -c 'inkscape $1 -w 48 -h 48 -o ../../ventoy/theme/adamsdesk/icons/${1%.svg}.png' _ {} \; || quit "Error: Unable to convert icons from SVG to PNG. Build has been halted."
    inkscape adamsdesk.svg -w 128 -h 91 -o ../../ventoy/theme/adamsdesk/adamsdesk.png || quit "Error: Unable to convert icons from SVG to PNG. Build has been halted."
}

prepare
build
quit 0
