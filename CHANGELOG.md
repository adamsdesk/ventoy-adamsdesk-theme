# Changelog
All notable changes to the project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.0] - 2024-02-06

### Added
- Haiku support
- HoloISO support
- ShredOS support
- Windows 11 support

### Changed
- Docs: bump version number for requirements
- Docs: document changes in changelog
- Docs: remove duplicate word
- Docs: update build script example
- Docs: update copyright year
- Docs: update icon attribution
- Update Windows 11 icon

### Fixed
- Ambiguous icon filename
- Docs: broken git clone link
- Docs: broken heading links

## [1.3.0] - 2021-02-14

### Changed
- Docs: update build result example (README.md).
- Docs: correct project name / paths (README.md).
- Docs: update screenshots (4).
    - Localboot menu
    - Power menu
    - Tools menu
    - TreeView
- Docs: document changes in changelog

## [1.2.0] - 2021-02-14

### Added
- Add the missing menu icons (29).

## [1.1.0] - 2021-02-13

### Removed
- Removed unused font, DejaVu Sans (6).

## [1.0.0] - 2021-02-13

Initial release.

[Unreleased]: https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme/commits/branch/main
[1.0.0]: https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme/compare/v1.0.0...main
[1.1.0]: https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme/compare/1.1.0...main
[1.2.0]: https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme/compare/1.2.0...main
[1.3.0]: https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme/compare/1.3.0...main
[1.4.0]: https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme/compare/1.4.0...main
