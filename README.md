# Ventoy Adamsdesk Theme

A simple Ventoy theme that mimics the website theme of [Adamsdesk.com](https://adamsdesk.com).

## Screenshots

### Main Menu

![Main Menu](screenshots/ventoy-ad-theme-main-menu.jpg)

### Memdisk

![Memdisk](screenshots/ventoy-ad-theme-memdisk.jpg)

### Power Menu

![Power Menu](screenshots/ventoy-ad-theme-power-menu.jpg)

### TreeView

![TreeView](screenshots/ventoy-ad-theme-treeview.jpg)

### Localboot Menu

![Localboot Menu](screenshots/ventoy-ad-theme-localboot-menu.jpg)

### Tools Menu

![Tools Menu](screenshots/ventoy-ad-theme-tools-menu.jpg)

### Grub Console

![Grub Console](screenshots/ventoy-ad-theme-console.jpg)

### Grub Boot Parameters

![Grub Boot Parameters](screenshots/ventoy-ad-theme-edit-params.jpg)

## Directory Structure

The directory structure listed below is in general terms and only those special files/directories are listed for
explanation purposes.

```console
.
├── screenshots/            < Screenshots of theme
├── src/                    < Referenced source build files
├── ventoy/                 < Temporary build directory
├── .gitignore              < Git ignore configuration
├── build.sh                < Build BASH script
├── CHANGELOG.md            < Project change log
├── CONTRIBUTING.md         < Project contributing guidelines
├── LICENSE                 < Project source code license
└── README.md               < Project read me
```

## Development

### Assumptions

- Have an understanding of general Linux practices.
- Have experienced working knowledge within a CLI (command-line interface).
- Using a Linux operating system.
- Installed all required dependencies as stated in [System Requirements](#system-requirements) section.
- Installation is done via Linux CLI.
- Steps prefixed with a "$" (dollar sign) represents the CLI prompt. The text
after the "$" is to be entered at the CLI.
- Steps prefixed with a "#" (number sign) represents the CLI prompt with
elevated user permissions (e.g. root). The text after the "#" is to be entered
at the CLI.
- A single backslash character beside another character is used to escape the
proceeding character. In this case backslash character is not to be entered
at the CLI.
- These instructions are an example of installation and configuration.

### System Requirements

- cp (GNU coreutils) v9.4
- find (GNU findutils) v4.9.0
- Git v2.43.0
- Inkscape v1.3.2
- mkdir (GNU coreutils) v9.4
- Ventoy v1.0.97

### Build Instructions

1. Download the project.
    ```console
    $ git clone https://git.nixnet.services/adamsdesk/ventoy-adamsdesk-theme.git
    ```
1. Change to project directory.
    ```console
    $ cd ventoy-adamsdesk-theme
    ```
1. Run build script.
    ```console
    $ ./build.sh
    ```
    ```console
    ventoy/
    ├── theme
    │   └── adamsdesk
    │       ├── adamsdesk.png
    │       ├── icons
    │       │   ├── alpinelinux.png
    │       │   ├── archlinux.png
    │       │   ├── boot_disk.png
    │       │   ├── boot_g4d.png
    │       │   ├── boot_uefi.png
    │       │   ├── boot_windows.png
    │       │   ├── cd.png
    │       │   ├── debian.png
    │       │   ├── debug_autoinstall.png
    │       │   ├── debug_automemdisk.png
    │       │   ├── debug_bootconf_replace.png
    │       │   ├── debug_control.png
    │       │   ├── debug_custom_boot.png
    │       │   ├── debug_dud.png
    │       │   ├── debug_gui_mode.png
    │       │   ├── debug_imageblacklist.png
    │       │   ├── debug_imagelist.png
    │       │   ├── debug_injection.png
    │       │   ├── debug_json.png
    │       │   ├── debug_kbd.png
    │       │   ├── debug_krdlayout.png
    │       │   ├── debug_menualias.png
    │       │   ├── debug_menuclass.png
    │       │   ├── debug_persistence.png
    │       │   ├── debug_pwd.png
    │       │   ├── debug_resolution.png
    │       │   ├── debug_screen_mode.png
    │       │   ├── debug_text_mode.png
    │       │   ├── debug_theme.png
    │       │   ├── debug_util.png
    │       │   ├── deepin.png
    │       │   ├── elementaryos.png
    │       │   ├── fedora.png
    │       │   ├── freedos.png
    │       │   ├── gentoo.png
    │       │   ├── haiku-os.png
    │       │   ├── holoiso.png
    │       │   ├── kali.png
    │       │   ├── kubuntu.png
    │       │   ├── linuxmint.png
    │       │   ├── linux.png
    │       │   ├── lubuntu.png
    │       │   ├── mageia.png
    │       │   ├── manjaro.png
    │       │   ├── openbsd.png
    │       │   ├── opensuse.png
    │       │   ├── power_halt.png
    │       │   ├── power_reboot.png
    │       │   ├── shredos.png
    │       │   ├── solus.png
    │       │   ├── ubcd.png
    │       │   ├── ubuntu-budgie.png
    │       │   ├── ubuntukylin.png
    │       │   ├── ubuntu-mate.png
    │       │   ├── ubuntu.png
    │       │   ├── ubuntustudio.png
    │       │   ├── vtoyret.png
    │       │   ├── windows-10.png
    │       │   ├── windows-11.png
    │       │   └── xubuntu.png
    │       ├── select_c.png
    │       ├── select_e.png
    │       ├── select_w.png
    │       ├── terminus-12.pf2
    │       ├── terminus-14.pf2
    │       ├── terminus-16.pf2
    │       ├── terminus-18.pf2
    │       └── theme.txt
    └── ventoy.json
    ```
1. Copy build directory to desired Ventoy installation.
    ```console
    $ cp -rp ventoy/ /mnt/usb/
    ```

## Attribution

I'm grateful for all the creators who have openly licensed their work to make this project possible.

### Fonts

- [Terminus Font](https://sourceforge.net/projects/terminus-font/)

### Icons

- [Alpine Linux](https://www.svgrepo.com/svg/305688/alpinelinux)
- [Arch Linux](https://www.svgrepo.com/svg/305739/archlinux)
- [Boot Disk](https://www.svgrepo.com/svg/55792/ssd)
- [Boot g4d](http://www.freedos.org/press-kit/logos.html)
- [Boot UEFI](https://upload.wikimedia.org/wikipedia/commons/d/df/Uefi_logo.svg)
- [Boot Windows](https://www.svgrepo.com/svg/54206/windows)
- [CD](https://www.svgrepo.com/svg/51861/compact-disc)
- [Debian](https://www.svgrepo.com/svg/305939/deepin)
- [Debug Autoinstall](https://www.svgrepo.com/svg/162186/text-page)
- [Debug Automemdisk](https://www.svgrepo.com/svg/41387/ram-memory)
- [Debug Bootconf Replace](https://www.svgrepo.com/svg/50331/switch)
- [Debug Control](https://www.svgrepo.com/svg/3637/global)
- [Debug Custom Boot](https://www.svgrepo.com/svg/71797/custom-coding)
- [Debug Dud](https://www.svgrepo.com/svg/136702/bug)
- [Debug GUI Mode](https://www.svgrepo.com/svg/275/open-computer-window)
- [Debug Imageblacklist](https://www.svgrepo.com/svg/205053/image-files-and-folders)
- [Debug Imagelist](https://www.svgrepo.com/svg/205063/image)
- [Debug Injection](https://www.svgrepo.com/svg/15130/injection)
- [Debug JSON](https://www.svgrepo.com/svg/8901/json-file)
- [Debug Keyboard](https://www.svgrepo.com/svg/702/keyboard)
- [Debug Menualias](https://www.svgrepo.com/svg/315576/shortcut)
- [Debug Menuclass](https://www.svgrepo.com/svg/16779/text-document)
- [Debug Persistence ](https://www.svgrepo.com/svg/55792/ssd)
- [Debug Pwd](https://www.svgrepo.com/svg/142566/password)
- [Debug Resolution](https://www.svgrepo.com/svg/9359/monitor)
- [Debug Screen Mode](https://www.svgrepo.com/svg/112276/computer-settings-interface-symbol-of-a-monitor-screen-with-gears)
- [Debug Text Mode](https://www.svgrepo.com/svg/162186/text-page)
- [Debug Theme](https://www.svgrepo.com/svg/311249/style-guide)
- [Debug Util](https://upload.wikimedia.org/wikipedia/commons/d/df/Uefi_logo.svg)
- [Deepin](https://www.svgrepo.com/svg/305938/debian)
- [ElementaryOS](https://www.svgrepo.com/svg/305993/elementary)
- [Fedora](https://freebiesupply.com/logos/fedora-logo-2/)
- [FreeDOS](http://www.freedos.org/press-kit/logos.html)
- [Gentoo](https://www.svgrepo.com/svg/306090/gentoo)
- [Haiku](https://commons.wikimedia.org/wiki/File:Haiku_(operating_system)_logo.svg)

    Converted to grayscale and then cropped to keep on the letter H with leaf to make legible.
- [Holoiso](https://user-images.githubusercontent.com/97450182/167457908-07be1a60-7e86-4bef-b7f0-6bd19efd8b24.png)

    Recreated logo as a vector in greyscale.
- [Kali Linux](https://seeklogo.com/vector-logo/285900/kali-linux)
- [Kubuntu](https://commons.wikimedia.org/wiki/File:Kubuntu_logo.svg)
- [Linux Mint](https://www.svgrepo.com/svg/306345/linuxmint)
- [Linux Penguin](https://www.svgrepo.com/svg/306344/linux)
- [Lubuntu](https://commons.wikimedia.org/wiki/File:Lubuntu_logo_only.svg)
- [Mageia](https://commons.wikimedia.org/wiki/File:Mageia_logo.svg)
- [Manjaro](https://www.svgrepo.com/svg/306371/manjaro)
- [OpenBSD](https://www.svgrepo.com/svg/306507/openbsd)
- [openSUSE](https://www.svgrepo.com/svg/306517/opensuse)
- [Power Halt](https://www.svgrepo.com/svg/19256/power-symbol)
- [Power Reboot](https://www.svgrepo.com/svg/56625/arrow-circle)
- [Return](https://www.svgrepo.com/svg/41182/left-arrow)
- [ShredOS](https://github.com/PartialVolume/shredos.x86_64/blob/master/images/shred_db.png)
- [Solus](https://www.svgrepo.com/svg/306754/solus)
- [Ubuntu Budgie](https://commons.wikimedia.org/wiki/File:Ubuntu_Budgie_No_Wordmark.svg)
- [Ubuntu Kylin](https://www.ubuntukylin.com/jintro/download.php?class1=464&lang=en) ([svg](https://www.ubuntukylin.com/public/images/version2019/images/UKlogo/5.svg))
- [Ubuntu Mate](https://commons.wikimedia.org/wiki/File:Ubuntu_MATE_rondel_logo.svg)
- [Ubuntu Studio](https://commons.wikimedia.org/wiki/File:Ubuntustudio_v3_logo_only.svg)
- [Ubuntu](https://www.svgrepo.com/svg/25424/ubuntu-logo)
- [Windows 10](https://www.svgrepo.com/svg/54206/windows)
- [Windows 11](https://www.svgrepo.com/svg/37912/windows-8)
- [Xubuntu](https://commons.wikimedia.org/wiki/File:Xubuntu_Icon.svg)

### Software

- [Git](https://git-scm.com/)
- [GNU Core Utilities](https://www.gnu.org/software/coreutils/)
- [GNU Findutils](https://www.gnu.org/software/findutils/)
- [GNU Grub](https://www.gnu.org/software/grub/)
- [Inkscape](https://inkscape.org/)
- [Ventoy](https://ventoy.net/)

## Copyright License

The copyright license (LICENSE) is only referring to the source code.

Adamsdesk logo Copyright (c) 2020 - 2024 Adam Douglas

All other logos / icons are copyright to those respected holders.

[Terminus Font](https://sourceforge.net/projects/terminus-font/) is licensed under the SIL Open Font License, Version
1.1. The license is included as OFL.TXT, and is also available with a FAQ at: https://scripts.sil.org/OFL
